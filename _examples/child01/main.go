package main

import (
	"fmt"
	"io"

	"gopkg.in/hedzr/errors.v3"
)

func main() {
	var err = errors.New("OK").WithErrors(io.EOF)
	println(err)
	println(io.EOF)
	fmt.Println(err)
	fmt.Println(io.EOF)
}
