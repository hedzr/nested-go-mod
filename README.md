# Can we make nested go.mod in a single Project?

Nop.

It is not a good practise.

For some repository with simple layout, you may make them
work, such as this one. But that's not stable.

Test me with:

```bash
$ go mod tidy
$ go build -v -o bin ./...     # It works
parent
parent/examples/child1

$ cd _examples
$ go mod tidy
$ go build -v -o ../bin ./...  # It works too.
```
