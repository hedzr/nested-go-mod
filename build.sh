
for gd in _examples examples; do
	pushd $gd && 
	for app in $(ls -db */); do
		go build -o ../bin/${app//\//} ./$app
		 done && 
	popd
done

go build -o bin/main .
